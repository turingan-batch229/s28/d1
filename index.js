db.users.insertOne({
    
        "username": "dahyunKim",
        "password": "once1234"
        
    })

db.users.insertOne({
    
        "username": "rjturingan",
        "password": "rjt1234"
        
    })

// Commands for Creating Documents Robo3T:
/*

        db.collectionName.insertOne([
                
                "field1": "value",
                "field2": "value"

        ])
        db.collectionName.insertMany([
                
                {
                        "field1": "valueA",
                        "field2": "valueA"
                },
                {
                        "field1": "valueB",
                        "field2": "valueB"
                }
        ])

*/

db.hotels.find({name:"double"})

//updateOne() - allows to update the first item that matches our criteria
//db.users.updateOne({username:"pedro99"},{$set:{username:"peter1999"}})
db.users.updateOne({},{$set:{username:"dahyunieTwice"}})

// Will add a new entry if it still has none.
//updateOne() - allows to update the first item that matches our criteria
//db.users.updateOne({username:"pedro99"},{$set:{username:"peter1999"}})
db.users.updateOne({username:"pablo123"},{$set:{isAdmin:true}})

//update.Many() - allows us to update all documents that matches that criteria
//db.users.updateMany({},{$set:{isAdmin:true}})
db.cars.updateMany({type: "sedan"},{$set:{price:1000000}})


//deleteOne() - deletes the first documeent that matches the criteria
//db.users.deleteOne({})
db.cars.deleteOne({brand:"Toyota"})

//deleteMany() - delete all items that matches the criteria
db.users.deleteMany({isAdmin: true})



